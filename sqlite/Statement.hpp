#pragma once

#include <utility>
#include <string>
#include <vector>
#include <unordered_map>
#include <exception>
#include <iostream>

#include "sqlite3.h"

namespace sqlite {
    class Field {
    public:
        Field(std::string name, const std::string &type, bool null = true, bool primary = false)
                : _name(std::move(name)),
                  _null(null),
                  _primary(primary) {
            if (type.find("CHAR") != std::string::npos) {
                this->_type = "CHAR";
                this->_size = std::stol(type.substr(5, type.size() - 2));
            } else {
                this->_type = type;
                this->_size = 0;
            }
        }

        std::string toSQL() const {
            return this->_name +
                   " " + this->_type +
                   (this->_size ? "(" + std::to_string(this->_size) + ")" : "") +
                   (this->_primary ? " PRIMARY KEY" : "") +
                   (this->_null ? "" : " NOT NULL");
        }

    private:

    protected:
        std::string _name;
        std::string _type;
        bool _primary;
        bool _null;
        long int _size;
    };

    class Statement {
    public:
        using Row = std::unordered_map<std::string, std::string>;
        using Rows = std::vector<Row>;

    public:
        Statement() = default;

        ~Statement() = default;

        void create(const std::string &tableName, const std::vector<Field> &fields, bool force = false) {
            this->_sql += "CREATE TABLE ";
            this->_sql += (force ? "" : "IF NOT EXISTS ") + tableName + "(";
            for (const auto &field : fields) {
                this->_sql += field.toSQL() + ",";
            }
            Statement::removeLastChar(this->_sql);
            this->_sql += ");";
        }

        void insert(const std::string &tableName, const Row &row) {
            std::string fields{};
            std::string values{};

            for (const auto &pair : row) {
                fields += pair.first + ",";
                values += "'" + pair.second + "',";
            }
            Statement::removeLastChar(fields);
            Statement::removeLastChar(values);

            this->_sql += "INSERT INTO " + tableName + "(" + fields + ") VALUES (" + values + ");";
        }

        Statement &select(const std::string &tableName, const std::string &what, sqlite3 *db) {
            this->_sql += "SELECT " + what + " FROM " + tableName + ";";
            this->_db = db;
            return *this;
        }

        Statement &update(const std::string &tableName, const std::string &what, sqlite3 *db) {
            this->_sql += "UPDATE " + tableName + " SET " + what + ";";
            this->_db = db;
            return *this;
        }

        Statement &remove(const std::string &tableName, sqlite3 *db) {
            this->_sql += "DELETE FROM " + tableName + ";";
            this->_db = db;
            return *this;
        }

        Statement &where(const std::string &literal) {
            Statement::removeLastChar(this->_sql);
            this->_sql += " WHERE " + literal + ";";
            return *this;
        }

        Statement &where(const std::unordered_map<std::string, std::string> &conditions) {
            Statement::removeLastChar(this->_sql);

            this->_sql += " WHERE ";
            for (const auto &condition : conditions) {
                this->_sql += condition.first + "='" + condition.second + "' AND ";
            }
            for (int i = 0; i < 5; ++i) {
                Statement::removeLastChar(this->_sql);
            }
            this->_sql += ";";
            return *this;
        }

        Statement &orderBy(const std::string &col, bool asc = true) {
            Statement::removeLastChar(this->_sql);
            this->_sql += " ORDER BY " + col + (asc ? " ASC;" : " DESC;");
            return *this;
        }

        Statement &orderBy(const std::unordered_map<std::string, bool> &cols) {
            Statement::removeLastChar(this->_sql);
            this->_sql += " ORDER BY ";

            for (const auto &col : cols) {
                this->_sql += col.first + (col.second ? " ASC," : " DESC,");
            }
            Statement::removeLastChar(this->_sql);
            this->_sql += ';';
            return *this;
        }

        void run(Rows &results) {
            this->exec(this->_db, Statement::callback, &results);
        }

        void exec(sqlite3 *db, sqlite3_callback cb = nullptr, void *param = nullptr) {
            sqlite3_exec(db, this->_sql.c_str(), cb, param, &(this->_errMsg));
            if (this->_errMsg)
                std::cerr << this->_errMsg << std::endl;
            this->clear();
        }

        void clear() {
            this->_sql.clear();
            if (this->_errMsg) {
                delete this->_errMsg;
                this->_errMsg = nullptr;
            }
            this->_db = nullptr;
        }

    private:
        static void removeLastChar(std::string &s) {
            s = s.substr(0, s.size() - 1);
        }

        static int callback(void *data, int argc, char **vals, char **cols) {
            auto results = static_cast<Rows *>(data);
            Row result;

            if (results) {
                for (int i = 0; i < argc; ++i) {
                    result.emplace(cols[i], vals[i] ? vals[i] : "NULL");
                }
                results->emplace_back(result);
            }
            return 0;
        }

    protected:
        std::string _sql;
        char *_errMsg{nullptr};
        sqlite3 *_db{nullptr};
    };
}