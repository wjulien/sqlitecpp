#pragma once

#include <utility>

#include "DataBase.hpp"

namespace sqlite {
    class Model {
    public:
        Model(DataBase &db, std::string name, std::vector<Field> fields, bool autoInc = true)
                : _db(db), _name(std::move(name)), _fields(std::move(fields)), _id(-1), _autoInc(autoInc) {
            if (autoInc) {
                Statement::Rows tmp;
                this->select("*").run(tmp);
                this->_id = tmp.size();
                Field id{"ID", "INT", false, true};
                this->_fields.emplace_back(id);
            }
            this->_db.create(this->_name, this->_fields);
        }

        virtual ~Model() = default;

    protected:
        virtual void insert(const Statement::Row &row) {
            if (this->_id >= 0) {
                auto withId = row;
                withId["ID"] = std::to_string(++(this->_id));
                this->_db.insert(this->_name, withId);
            } else {
                this->_db.insert(this->_name, row);
            }
        }

        virtual void bulkInsert(const Statement::Rows &rows) {
            if (this->_id >= 0) {
                for (const auto &row : rows) {
                    auto withId = row;
                    withId["ID"] = std::to_string(++(this->_id));
                    this->_db.insert(this->_name, withId);
                }
            } else {
                this->_db.bulkInsert(this->_name, rows);
            }
        }

        virtual Statement &select(const std::string &what) const {
            this->_db.select(this->_name, what);
        }

        virtual Statement &update(const std::string &what) const {
            this->_db.update(this->_name, what);
        }

        virtual Statement &remove() const {
            this->_db.remove(this->_name);
        }

    private:
        int _id;
        bool _autoInc;

    protected:
        std::string _name;
        DataBase &_db;
        std::vector<Field> _fields;
    };
}