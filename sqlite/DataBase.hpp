#pragma once

#include <string>

#include "sqlite3.h"

#include "Statement.hpp"

namespace sqlite {

    class DataBase {
    public:
        explicit DataBase(const std::string &filename) :_db(nullptr) {
            if (sqlite3_open(filename.c_str(), &(this->_db))) {
                throw "Cannot open db " + filename + sqlite3_errmsg(this->_db);
            }
        }

        ~DataBase() {
            sqlite3_close(this->_db);
        }

        void create(const std::string &tableName, const std::vector<Field> &fields) {
            this->_stmt.clear();
            this->_stmt.create(tableName, fields);
            this->exec();
        }

        void insert(const std::string &tableName, const Statement::Row &row) {
            this->_stmt.clear();
            this->_stmt.insert(tableName, row);
            this->exec();
        }

        void bulkInsert(const std::string &tableName, const Statement::Rows &rows) {
            this->_stmt.clear();
            for (const auto &row : rows) {
                this->_stmt.insert(tableName, row);
            }
            this->exec();
        }

        Statement &select(const std::string &tableName, const std::string &what) {
            this->_stmt.clear();
            this->_stmt.select(tableName, what, this->_db);
        }

        Statement &update(const std::string &tableName, const std::string &what) {
            this->_stmt.clear();
            this->_stmt.update(tableName, what, this->_db);
        }

        Statement &remove(const std::string &tableName) {
            this->_stmt.clear();
            this->_stmt.remove(tableName, this->_db);
        }

    private:
        void exec() {
            this->_stmt.exec(this->_db);
        }

    private:
        sqlite3 *_db;
        Statement _stmt;

    protected:

    };

}