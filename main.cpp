#include <iostream>

#include "sqlite/DataBase.hpp"
#include "src/TableCompany.hpp"

static void printEmployees(const TableCompany &company) {
    auto employees = company.employees();
    for (const auto &employee : employees) {
        std::cout << "{ ";
        for (const auto &property : employee) {
            std::cout << property.first << ": " << property.second << "\t";
        }
        std::cout << "}" << std::endl;
    }
}

int main() {
    sqlite::DataBase db("db.sqlite");
    TableCompany company(db);

    company.addEmploye("Jack", 32);
    printEmployees(company);
    company.addEmploye("Jack", 32);
    company.addEmploye("Jack", 32);
    company.addEmploye("Jack", 32);
    company.addEmploye("Jack", 32);
    company.addEmploye("Jack", 32);
    company.addEmploye("Jack", 32);
    company.addEmploye("Jack", 32);
    company.addEmploye("Jack", 32);
    company.addEmploye("Jack", 32);
    company.addEmploye("Jack", 32);
    printEmployees(company);

    return 0;
}