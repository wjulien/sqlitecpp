#pragma once

#include "../sqlite/Model.hpp"

class TableCompany : public sqlite::Model {
public:
    explicit TableCompany(sqlite::DataBase &db)
            : Model(db, "COMPANY", {
                { "NAME", "TEXT", false },
                { "AGE", "INT", false },
                { "ADDRESS", "CHAR(50)" },
                { "SALARY", "REAL" },
            }) {

    }

    void addEmploye(const std::string &name, int age, const std::string &address = "", double salary = -1) {
        sqlite::Statement::Row employee = {
                { "NAME", name },
                { "AGE", std::to_string(age) }
        };

        if (!address.empty()) {
            employee.emplace("ADDRESS", address);
        }
        if (salary > 0) {
            employee.emplace("SALARY", std::to_string(salary));
        }
        this->insert(employee);
    }

    auto employees() const {
        sqlite::Statement::Rows ret;
        this->select("*").run(ret);
        return ret;
    }

private:

};