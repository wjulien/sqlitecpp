# Table of content

# What for?
Simplifying the db interaction in c++ with a way to chain function calls to customize you request, ...

# How to use
Copy paste the sqlite dir in your project and set up your cmake / makefile to compile the .c file

## Code samples

Inherinting the Model class will give you a class with already implemented mechanics

```cpp
class MyModel : public sqlite::Model {
public:
    MyModel(sqlite::DataBase &db)
     : Model(db, "my_model_name", {
        { "NAME", "TEXT", false, false }
     }){
    }
    
    auto selectAll() const {
        sqlite::Statement::Rows ret;
        this->select("*").run(ret);
        return ret;
    }
    
    auto selectByName(const std::string &name) const {
        sqlite::Statement::Rows ret;
        //this->select("*").where("NAME="+name).run(ret); litteral where
        this->select("*").where({{"NAME", name}}).run(ret); map of equality conditions
        return ret;
    }
    
    void insert(const std::string &name) {
        this->insert({{"NAME", name}});
    }
};
```

The db parameter is your database that you can very simply create like this :
```cpp
sqlite::DataBase db("./mydb.db"); // Path of the file you want to write
```
Here is a main file that will show you some uses.
```cpp

static void printRows(const sqlite::Statement::Rows &rows) {
    for (const auto &row : rows) {
        std::cout << "{ ";
        for (const auto &property : row) {
            std::cout << property.first << ": " << property.second << "\t";
        }
        std::cout << "}" << std::endl;
    }
}

int main() {
    sqlite::DataBase db("./mydb.db"); // Path of the file you want to write
    MyModel model(db);
    
    auto data = model.selectAll();
    printRows(data);
    printRows(model.selectByName("Robert"));
    model.insert("Robert");
    model.insert("John");
    printRows(model.selectAll());
    printRows(model.selectByName("Robert"));
    
    return 0;
}
```

Notice the id field in the output?

The Model class has a autoInc parameter defaulting to true that adds an id collumn to all your tables.
You can disable this by just creating your model as such : 
```cpp
    MyModel(sqlite::DataBase &db)
     : Model(db, "my_model_name", {
        { "NAME", "TEXT", false, false }
     }, false){ // Notice the false here?
    }
```

By the way, the third parameter is a std::vector of Field, which is a class that helps build your model in sql.
In the future it will also help getting typed data and not strings, but I have to finish my jsoncpp project first 